This is more or less a flashcard app. The point is to go out and engage with the actual world (through news sites, social networking sites, etc.), and then consciously take part in collecting parts of the language that feel unintuitive. That is, take notes. The notes can be words or phrases.

Each "card" should get

- a word or a phrase
- the parts of speech for each word (can this be done with stanbol/opennlp? or should it be done by hand?)
- ? an audio file of its pronunciation (link or actual data? yourself or someone else saying it?)
- tags

create table card (
  id serial primary key,
  phrase text not null,
  audio bytea
);

create table tag (
  id serial primary key,
  card_id int not null,
  label text not null
);

create table token (
  id serial primary key,
  card_id int not null,
  index int not null,
  pos text not null
);

hiatus. Whatever. I think I need to think about how I want to interact with it. I run the program, what happens? I'm given a word or a phrase, and the sentence it comes from. I write the translation down, then I hit enter. The recorded translation then appears underneath. It doesn't check it, it just presents it for my study benefit.

9/19/2015 -- I made a first crack at "linnea" a week ago and managed to get a CLI working, but I'm thinking about the interface some more, and taking a deep dive into semantic web technologies again. I'm thinking about making it much more of a process. The _main_ idea, though, is that I'm trying to concentrate on not just the language in general, but the part of the language I plan to use. I think the best way to do that is by thinking about what you want to say, trying to express yourself, and reading material in the language from sources that interest you. Those are the two use-cases. 

So that said, the interface should be similar for both, but attacked from a different perspective.

For the first use-case, a more detailed description of the use-case might be:

You have someone you want to write to in a different language. You think of something you want to say. You enter the complete sentence into linnea in your source language. You tell it what your target language is. At this point, you should have the option of sending the source to a complete translation service, at which point you want to be able to check the translation, by querying dbpedia, maybe. But the simpler way to go about this might be to send each source word to dbpedia to get a list of possible translations. And then you can spend some time trying to put the sentence together. Some of these queries take over a minute, though, which is of course too long.

hiatus. I probably should be thinking in terms of modes. You have a composition mode, and a translation mode. In either mode, you should be able to create flashcards, right? In composition mode, you are trying to write the way you think--and in the process you're creating translations in the target language. It may not be possible for you to be without a translation service, just because it would take so long for the queries to work. I mean, you can't just look up translations from the wiktionary api. However, once you have a translation to work with, you can use the wiktionary api to study the words that it gives you. In the case of "translation mode," you can directly use the wiktionary to come up with a really good translation, and then submit pieces of that as flashcards. This is quite a lot more involved than I hoped it would be.


#### below is all good:

hiatus. Think about the interface. Think about how you want the experience to go. I want to write to Linnea. I want to be able to write to her in my own words, or what I feel are closest to my own words. So the easiest thing to do would be to use yandex to translate complete sentences, and then maybe the cards could be those complete thoughts, with the aid of dbpedia providing context/additional meanings.

So that sounds pretty solid, actually. What about the other use case? Isn't that basically the same? Except instead of entering a source language, you jump straight to the target language, and then the target gets made into a "card" in exactly the same way.

Also, it occurs to me that "composition" mode (i.e., joining a string of sentences, basically), would be the same process in either direction, too. Or a very similar process anyway.

Random card, list all cards, search cards, tag cards, mark progress.

1. Composition mode
    - add a card/sentence (choose the direction--from source or from target)
    - select a word or phrase to get an analysis back from web-sources

2. Study mode


hiatus again. I've found that there are already some resources out there that are trying to be global dictionaries and repositories for example sentences. That's sort of the major point of this, so it seem like it would be a good thing to send your translations there. The thing that's more interesting for me in terms of an application is to be able to apply the translation to my own creativity. My own thoughts. Yes, you can add one sentence to tatoebo and then an explosion of translations follows, but why would add a sentence to tatoebo in the first place? I think there needs to be a creative point to this, and one that is geared toward helping people to learn the languages they're translating.

Part of the problem with RDF and RDF ontologies is that they're difficult to find and there's no real documentation on them. Each one seems to ignore some vocabs while embracing others and sme that are embraced seem off-putting in some way. Like there's something imprecise about the terms. And you want each term to be as precise as possible, but that's sort of the problem: it's just like a natural language in that one word might have several senses, and when you're dealing with an ontology that is a dictionary, that confusion is really amplified. The thing is there are richers apis than dbpedia, which just aren't in an RDF format. You'd have to reinvent the RDF to be used for those things, which you could, and then later you could rdf:sameAs it, but that's a lot of maintenance.

So the bottom line is that I need to design a model that I save locally, and that can interact with these other sources. I also want to be able to contribute to them, if I can. I think since the tatoeba database is already being used by glosbe, I should just use their api (which is already available). If I have to translate these into RDF for some reason, I can. The only difference between a key in a dictionary and an RDF predicate is that they're precise in different ways. One aims to be precise in a global scope. The other aims to be precise in a local scope. Swap precise for accessible, because really that's what precision implies.

Anyway, going back to my own  purposes, I just want to write, mostly. So let's tackle that first. I think I've decided that I want to _start by adding a "card," which is like a flash card. A target sentence is provided on the "front" of the card, and the source, along with other information mediated by the source language is on the back._ You can enter a card in the target language or the source language. If it's "from source," do you use a translation service to translate the whole phrase? Or do you use glosbe to translate one word at a time until you've come up with your own translation? Do you provide both?

_I don't think the resources out there for single word or phrase translation are powerful enough yet, so I'm going to use a translation service to create the card, and use the other services for studying and revising._


