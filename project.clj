(defproject linnea "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :main linnea.cli.core
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [org.postgresql/postgresql "9.4-1200-jdbc41"]
                 [org.clojure/java.jdbc "0.4.1"]
                 [me.raynes/conch "0.8.0"]
                 [org.fusesource.jansi/jansi "1.11"]
                 [compojure "1.4.0"]
                 [cheshire "5.5.0"]
                 [ring/ring-defaults "0.1.5"]
                 [clj-http "2.0.0"]
                 [batik "0.1.0-SNAPSHOT"]
                 [com.thinkaurelius.titan/titan-all "0.5.4"]
                 [com.thinkaurelius.titan/titan-cassandra "0.5.4"
                  :exclusions [com.google.guava/guava]
                  ]
                 [com.google.guava/guava "15.0"]
                 [com.tinkerpop.blueprints/blueprints-graph-sail "2.6.0"]
                 [org.flatland/ordered "1.5.2"]
                 ]
  :plugins [[lein-ring "0.9.6"]]
  :ring {:handler linnea.rest/app}
  )
