(ns linnea.cli.common
  (:require [flatland.ordered.map :refer [ordered-map]])
  (:import (org.fusesource.jansi Ansi Ansi$Color)))

(declare commands)
(declare run)
(declare help)


(defn cls
  [& _]
  (println (-> (Ansi/ansi) .eraseScreen str))
  true)

(defmacro str-color
  [color & argv]
  `(-> (Ansi/ansi)
    (.fg Ansi$Color/DEFAULT)
    (.fg ~(symbol
           (str "Ansi$Color/"
                (.toUpperCase (name color)))))
    (.a (str ~@argv))
    (.fg Ansi$Color/DEFAULT)
    .reset
    str))


(defn swap
  [conf]
  (let [src @(-> conf :source)
        tgt @(-> conf :target)]
    (swap! (-> conf :source) (fn [old] tgt))
    (swap! (-> conf :target) (fn [old] src)))
  true)


(defn valid-lang?
  [code]
  (or (= (count code) 2)
      (clojure.string/blank? code)))


(defn set-lang
  [which conf]
  (let [lang (first (-> conf :args))]
    (if (not (valid-lang? lang))
      (println (str-color :red
                 (str (name which) " must be the two-character ISO 639-1 language code"))
               (str
                 "\n\tplease see"
                 (str-color :yellow " https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes")))
      (condp = which
        :source (swap! (-> conf :source)
                       (fn [old] lang))
        :target (swap! (-> conf :target)
                       (fn [old] lang))))
      true))


(defn set-source
  [conf]
  (set-lang :source conf))


(defn set-target
  [conf]
  (set-lang :target conf))


(defn set-langs
  [conf]
  (let [src (-> conf :args first)
        tgt (-> conf :args second)]
    (if (some clojure.string/blank? [src tgt])
      (do (println (str-color :red "requires both a source and a target language code"))
          true)
      (if (every? valid-lang? [src tgt])
        (do
          (swap! (-> conf :source) (fn [old] src))
          (swap! (-> conf :target) (fn [old] tgt)))
        (println (str-color :red "invalid source or target"))))
      true))


(defmacro println-color
  [color & argv]
  `(println
    (-> (Ansi/ansi)
      (.fg ~(symbol
             (str "Ansi$Color/"
                  (.toUpperCase (name color)))))
      (.a (str ~@argv))
      (.fg Ansi$Color/DEFAULT)
      str)))


(defn get-cmd
  [commands cmdstr]
  (or (->> commands
        (filter #(-> % second first name (= cmdstr)))
        first second)
      (->> commands
        (filter #(some (fn [x] (.startsWith (name x) cmdstr))
                         (-> % second second)))
        first second)))


(defn help
  [conf]
  (print (str "Enter one of the following commands:\n"
         (clojure.string/join "\n"
           (map
             #(apply
               (fn [abbr opts args explanation & _]
                 (str (str-color :cyan abbr)
                      (when-not (empty? opts)
                        (str (str-color :default " or ") (clojure.string/join ", " (map (fn [o] (str-color :cyan o)) opts))))
                      (when-not (empty? args)
                        (str (str-color :default " => " (clojure.string/join ", " (map (fn [a] (str-color :magenta a)) args)))))
                      " | "
                      (str-color :yellow explanation)))
               %)
             (map second (-> conf :commands))))
         "\n"))
  true)


(defn read-cmd-args
  []
  (let [line (read-line)
        cmdstr (-> line
                 (clojure.string/split #" ")
                 first clojure.string/trim
                 .toLowerCase)
        args (->> (clojure.string/split line #" ")
                  (drop 1)
                  (map #(clojure.string/trim %)))]
     [cmdstr args]))


(defn print-prompt
  [& [conf]]
    (print (-> (Ansi/ansi)
             .bold
             (.fg Ansi$Color/MAGENTA)
             (.a (str (when (-> conf :mode) (str (-> conf :mode) " "))))
             (.a (str "[" @(-> conf :source)))
             (.a ":")
             (.a (str @(-> conf :target) "]"))
             (.a "$ ")
             (.fg Ansi$Color/DEFAULT)
             .reset
             str))
    (flush))


(def commands
  (ordered-map
    :help ["h" ["help"] [] "print this menu" help]
    :source ["s" ["source"] ["lang"] "set the source language for the session" set-source]
    :target ["t" ["target" "destination"] ["lang"] "set the target language for the session" set-target]
    :langs ["l" ["langs" "languages"] ["source-lang" "target-lang"] "set both the source and the target for the session in the same command" set-langs]
    :swap ["sw" ["swap" "switch"] [] "swap the source and target for the session" swap]
    :clear ["cls" ["clear"] [] "clear the screen" cls]
  ))


(defn run
  [conf]
  (loop []
    (print-prompt conf)
    (let [[cmdstr args] (read-cmd-args)]
      (if (clojure.string/blank? cmdstr)
        (recur)
        (if-let [cmd (get-cmd (-> conf :commands) cmdstr)]
          (condp = ((last cmd) (assoc conf :args args))
            true (recur)
            false (if (-> conf :mode) false (recur))
            :back true
            nil)
          (do
            (println (str-color :red "command not found"))
            (recur)))
            ))))

