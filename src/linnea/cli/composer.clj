(ns linnea.cli.composer
  (:require [linnea.cli.common :refer [print-prompt read-cmd-args help str-color run] :as common]
            [linnea.graph :as graph]
            [flatland.ordered.map :refer [ordered-map]])
  (:import (org.fusesource.jansi Ansi Ansi$Color)))


(def document (atom nil))
(def cards (atom []))
(def documents (atom []))


(defn print-cards
  [conf & [dir]]
  (if-let [lang @(conf (or dir :source))]
    (let [targets (graph/target (:uri @document) lang)]
      (println
        (clojure.string/join " "
          (map-indexed
            #(str (->> %2 :text .stringValue (str-color :blue) (str "[" (str-color :cyan %1) "]")))
            targets))))
    (println (str-color :red "No language supplied")))
  true)

;  (let [cards (map-indexed
;                #(str "[" (str-color :cyan %1) "]"
;                      (when-let [lang @(conf (or dir :source))]
;                        (str-color :green (when-let [v (->> lang (graph/targets %2) first :text)] (.stringValue v)))))
;                (-> @document :cards))]
;    (println (clojure.string/join " " cards)))
;  true)


(defn print-target
  [conf]
  (print-cards conf :target))


(defn append
  [conf]
  (loop []
    (print-cards conf)
    (let [line (clojure.string/trim (read-line))]
      (if-not (clojure.string/blank? line)
        (let [card (graph/card! line @(-> conf :source) @(-> conf :target))
              doc (:uri @document)]
          (graph/append-to-document! doc -1 card)
          (swap! document #(assoc % :cards (graph/get-cards-for-document doc)))
          (recur))
        true))))


(defn list-documents
  [conf]
  (println "list docs")
  (let [docs (graph/documents)]
    (swap! documents (fn [old] docs))
    (doall (map-indexed
      (fn [idx doc]
        (println (str "[" (str-color :cyan idx) "] " (str-color :green (:title doc)))))
      docs)))
  true)

(defn count-cards
  [conf]
  (println (str "Number of cards in '" (.stringValue (:title @document))  "': " (-> @document :cards count)))
  true)

(defn open-document [conf] (println "open")
  (when-not (empty? (-> conf :args))
    (let [idx (-> conf :args first Integer/parseInt)]
      (if (> (count @documents) idx)
        (swap! document (fn [old] (let [doc (-> @documents (nth idx))]
                                    (assoc doc :cards (graph/get-cards-for-document (:uri doc))))))
        (println (str-color :red "No document found at that index")))))
  (println (str-color :cyan "current document:") (-> @document :title .stringValue))
  true)

(defn new-document [conf] (println "new") true)
(defn commit-document [conf] (println "commit/save") true)
(defn tag-document [conf] (println "tag") true)


(def commands
  (merge
    (ordered-map
      :documents ["ld" ["docs" "documents"] ["tag1" "tag2" "etc. (optional)"] "list all available documents" list-documents]
      :count ["cc" ["count-cards"] [] "list the cards for the current document" count-cards]
      :open-document ["o" ["open"] ["document index (optional)"] "open a saved document" open-document]
      :new-document ["n" ["new"] [] "create a new document" new-document]
      :append ["a" ["append"] ["position (optional)"] "append some text to the document" append]
      :save ["sa" ["save" "commit"] ["title (optional--same as 'save as')"] "save the document and all of its phrases to the database" commit-document]
      :tag ["ta" ["tag"] ["text"] "tag the current document" tag-document]
      :print-source ["p" ["print"] [] "print the current document in the source language" print-cards]
      :print-target ["pt" ["print-target"] [] "print the current document in the target language" print-target]
      :push ["push" [] [] "push the current state onto the stack" run]
      :back ["pop" ["back"] [] "pop the current state off of the stack; go back" (constantly :back)]
      :quit ["q" ["quit"] [] "exit to the main menu" (constantly false)]
      :exit ["exit" [] [] "exit the program" (constantly nil)]
      )
    common/commands))


(defn composer
  [conf]
  (println (str-color :cyan "Type 'h' or 'help' for a list of options"))
  (run (assoc conf :mode "compose" :commands commands)))

