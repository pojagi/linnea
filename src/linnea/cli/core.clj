(ns linnea.cli.core
  (:require [clojure.java.jdbc :as j]
            [clojure.core.async :refer [>!! >! <! go-loop chan go]]
            [me.raynes.conch :as sh]
            [linnea.graph :refer [card! *dry-run*]]
            [linnea.cli.common :refer [print-prompt read-cmd-args help str-color run get-cmd cls] :as common]
            [linnea.cli.composer :refer [composer]]
            [flatland.ordered.map :refer [ordered-map]]
            )
  (:import (org.fusesource.jansi AnsiConsole)
           (org.fusesource.jansi Ansi Ansi$Color)))

(declare commands)

(def play-chan (chan))
(def source-lang (atom nil))
(def target-lang (atom nil))


(defn play
  [tmp]
  (if-not (nil? tmp)
    (>!! play-chan tmp)
    (println (str-color :cyan "(no audio available)"))))


(defn play*
  [tmp]
  (sh/with-programs [afplay]
    (afplay (-> tmp .getCanonicalPath))))


;;(go-loop []
;;  (let [tmp (<! play-chan)]
;;    (play* tmp)
;;    (recur)))


(defn multicolor-str
  [& argv]
  (let [seed (-> (java.util.Date.) .getTime)]
    (apply str
      (map-indexed
        (fn [idx string]
          (condp #(zero? (mod %2 %1)) (+ idx seed)
            6 (str-color :red string)
            5 (str-color :magenta string)
            4 (str-color :white string)
            3 (str-color :yellow string)
            2 (str-color :blue string)
            1 (str-color :green string)
            ))
        (mapcat identity argv)))))


(defn bold [x] (-> (Ansi/ansi) .bold (.a x) .boldOff str))
(defn reset [x] (-> (Ansi/ansi) .reset (.a x) str))
(defn goodbye [& _]
  (println (bold (multicolor-str "Hej då!!!!!"))))


(defn cline
  []
  (println (-> (Ansi/ansi) .eraseLine (.scrollDown 1) str)))


(defn pphrase
  [row]
  (println (-> (Ansi/ansi) .bold (.fg Ansi$Color/BLUE) (.a (-> row :source)) (.fg Ansi$Color/DEFAULT) str)))


(defn ptranslations
  [row]
  (doseq [translation (-> row :translations)]
    (println (bold (-> (Ansi/ansi) (.fg Ansi$Color/GREEN) (.a (-> translation :translation)) (.fg Ansi$Color/DEFAULT) str)))))


(defn flashcards 
  [conf]
  (println "flashcards")
  true)


(def commands
  (merge
    (ordered-map
      :compose ["c" ["compose"] [] "run the composer program" composer]
      :study ["f" ["flashcards"] [] "run the flashcard program" flashcards]
      :quit ["q" ["quit" "exit"] [] "quit" (constantly nil)])
    common/commands
    ))


(defn -main
  [& argv]
  (AnsiConsole/systemInstall)
  (binding [*dry-run* true]
    (cls)
    (print (bold (multicolor-str "LINNEA")) "\n")
    (println (str-color :cyan "Type 'h' or 'help' for a list of options"))
    (run {:commands commands :source (atom "en") :target (atom "sv")}))
  (goodbye)
  (println (-> (Ansi/ansi) (.bg Ansi$Color/DEFAULT) str)))


;;    ["l" "listen" "listen to a text-to-speech rendition of the card"]
;;    ["t" "translation" "print the translations for the card"]
;;    ["n" "next" "go to the next card"]
;;    ["p" "print" "print the current card"]
;;    ["tl" "translate" "verify your translation attempt"]
;;    ["ts" "transcribe" "verify your transcription attempt"]
;;    ["add-source" nil "add a card by providing the source language"]
;;    ["add-target" nil "add a card by providing the target language"]
;;    ["cls" nil "clear the screen"]


;;#_(defn run-card
;;  [& ids]
;;  (let [row (first (random-cards :exclude ids :limit 1 :audio true))]
;;    (if-not row
;;      (do
;;        (println (str-color :cyan "No more cards...start over (Y, n, add-source, add-target)? "))
;;        (let [resp (read-line)]
;;          (condp = (.toLowerCase resp)
;;                "y" (recur [])
;;                "n" (goodbye)
;;                "add-source" (do (run-add-source) (recur []))
;;                "add-target" (do (run-add-target) (recur []))
;;                (recur [])
;;                )))
;;      (do
;;        (let [tmp (when (-> row :audio)
;;                    (let [tmp (java.io.File. (str ".tmp" (.getTime (java.util.Date.)) ".wav"))
;;                          fos (java.io.FileOutputStream. tmp)]
;;                      (->> row :audio (.write fos))
;;                      (.close fos)
;;                      tmp))]
;;          (loop []
;;            (print (-> (Ansi/ansi) .bold (.fg Ansi$Color/MAGENTA) (.a "$ ") (.fg Ansi$Color/DEFAULT) str))
;;            (flush)
;;            (let [entry (.toLowerCase (read-line))]
;;              (cline)
;;              (cond
;;
;;                (= "" entry)
;;                (recur)
;;                #_(do
;;                  (pphrase row)
;;                  (play tmp)
;;                  (recur))
;;
;;                (= entry "cls")
;;                (do (cls) (recur))
;;
;;
;;                (= entry "add-source")
;;                (do (run-add-source) (recur))
;;
;;
;;                (= entry "add-target")
;;                (do (run-add-target) (recur))
;;
;;
;;                (or (= "q" entry)
;;                    (= "exit" entry)
;;                    (-> "quit" (.startsWith entry)))
;;                (goodbye)
;;
;;
;;                (or (= "h" entry)
;;                    (-> "help" (.startsWith entry)))
;;                (do (help) (recur))
;;
;;
;;                (or (= "p" entry)
;;                    (-> "print" (.startsWith entry)))
;;                (do
;;                  (pphrase row)
;;                  (recur))
;;
;;
;;                (or (= "l" entry)
;;                    (-> "listen" (.startsWith entry)))
;;                (do
;;                  (play tmp)
;;                  (recur))
;;
;;
;;                (or (= "t" entry)
;;                    (-> "translation" (.startsWith entry)))
;;                (do
;;                  (ptranslations row)
;;                  (recur))
;;
;;
;;                (or (= "n" entry)
;;                    (-> "next" (.startsWith entry)))
;;                (apply run-card (conj ids (-> row :id)))
;;
;;
;;                (or (= "tl" entry)
;;                    (-> "translate" (.startsWith entry)))
;;                (do
;;                  ((fn []
;;                    (let [attempt (read-line)]
;;                      (when-not (= attempt "q")
;;                        (let [success (some true?
;;                                        (for [translation (-> row :translations)]
;;                                          (= (.toLowerCase attempt)
;;                                          (.toLowerCase (-> translation :translation)))))]
;;                          (if success
;;                            (do (println (bold (multicolor-str "SUCCESS!!!"))))
;;                            (do (println-color :red "try again (or enter 'q' to return to the main prompt)")
;;                                (recur))))))))
;;                  (recur))
;;                        
;;
;;                (or (= "ts" entry)
;;                    (-> "transcribe" (.startsWith entry)))
;;                (do
;;                  ((fn []
;;                    (let [transl (read-line)]
;;                      (condp = (.toLowerCase transl)
;;                        (.toLowerCase (-> row :source))
;;                        (do
;;                          (println-color :yellow "success!"))
;;                        "q" nil
;;                        (do
;;                          (println-color :red "try again (or enter 'q' to return to the main prompt)")
;;                          (recur))))))
;;                  (recur))
;;
;;                :else (do
;;                        (println-color :red "command not found")
;;                        (recur)))))
;;          (when tmp (.delete tmp)))
;;         ))))



