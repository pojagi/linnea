(ns linnea.graph
  (:require [me.raynes.conch :as sh]
            [cheshire.core :as json]
            [batik.core :refer [with-sc uri bnode node literal add-statement remove-statements get-statements iter query append-items resource-str indv]]
            [clj-http.client :as client]
    )
  (:import (org.openrdf.model.vocabulary RDF RDFS)))


(def ^:dynamic *dry-run* true)
(def yandex-api-key "trnsl.1.1.20150913T155020Z.f35e0e0e6d64a47d.4e708c9d0745a038e5beae2bf6fdad1356d99cc3")
(def yandex_translate-base (str "https://translate.yandex.net/api/v1.5/tr.json/translate?key=" yandex-api-key "&"))
(def resources "http://linnea.nclk.co/resources")

(defn lt
  [& more]
  (uri (apply resource-str (conj more "http://linnea.nclk.co/terms#"))))

(defn lr
  [& more]
  (uri (apply resource-str (conj more "http://linnea.nclk.co/resources#"))))


(defn card!
  [source & targets]
  (with-sc conn
    (let [from (first targets)
          card (indv resources "cards")
          source-target (indv resources "targets")]
      (-> conn
        (add-statement card RDF/TYPE (lt :Card) (uri resources))
        (add-statement source-target RDF/TYPE (lt :Target) (uri resources))
        (add-statement source-target (lt :targetOf) card (uri resources))
        (add-statement source-target (lt :text) (literal source from) (uri resources)))
      (doseq [to (drop 1 targets)]
        (let [target-target (indv resources "targets")
              target-text (let [resp (client/get
                                       (str yandex_translate-base
                                            "lang=" from "-" to
                                            "&text=" (java.net.URLEncoder/encode source "UTF-8"))
                                       {:accept :json})]
                            (-> resp :body (json/parse-string true) :text first))]
          (-> conn
            (add-statement target-target RDF/TYPE (lt :Target) (uri resources))
            (add-statement target-target (lt :targetOf) card (uri resources))
            (add-statement target-target (lt :text) (literal target-text to) (uri resources)))
          ))
      (-> conn .commit)
      card)))


(defn target
  [doc lang]
  (with-sc conn
    (let [querystr (str "select distinct ?card ?target ?text from <http://linnea.nclk.co/resources> where { "
                        "<" doc "> rdf:rest*/rdf:first ?card . "
                        "?target <#targetOf> ?card . "
                        "?target <#text> ?text . "
                        "filter langMatches(lang(?text), '" lang "') . "
                        "}")
          results (-> conn
                    (query querystr "http://linnea.nclk.co/terms" false))]
      (doall
        (for [bindings results]
          (let [text (-> bindings (.getBinding "text") .getValue)]
            {:uri (-> bindings (.getBinding "target") .getValue)
             :text text
             :lang (-> text .getLanguage)}))))))


(defn targets
  [card & langs]
  (with-sc conn
    (let [res (-> conn
                (query
                  (str "select ?target ?text from <http://linnea.nclk.co/resources> where { "
                       (clojure.string/join " UNION "
                         (map
                           #(str "{ ?target <#targetOf> <" card ">. ?target <#text> ?text. filter langMatches(lang(?text), '" % "') }")
                           (remove nil? langs)))
                       "}")
                  "http://linnea.nclk.co/terms" false))]
      (doall
        (for [s res]
          (let [text (-> s (.getBinding "text") .getValue)]
            {:uri (-> s (.getBinding "target") .getValue)
             :text text
             :lang (-> text .getLanguage)})))
      )))


(defn get-cards-for-document
  [doc]
  (with-sc conn
    (doall
      (map
        #(-> % (.getBinding "card") .getValue)
        (-> conn
          (query (str "select ?card from <http://linnea.nclk.co/resources> where { <" doc "> rdf:rest*/rdf:first ?card }")
                      "http://linnea.nclk.co/terms" false))))))


(defn append-to-document!
  [doc idx & cards]
  (when-not (empty? cards)
    (with-sc conn
      (let [wstr (condp = idx
                   0 (str "<" doc "> rdf:rest ?next.")
                   -1 (str "<" doc "> rdf:rest*/rdf:rest ?prev. ?prev rdf:rest rdf:nil .")
                   (str "<" doc "> " (clojure.string/join "/" (for [x (range idx)] (str "rdf:rest"))) " ?prev . "
                        "?prev rdf:rest ?next . "))
            bindings (first
                       (query conn
                         (str "select ?prev ?next "
                              "from <http://linnea.nclk.co/resources> where { " wstr "}")
                         "http://linnea.nclk.co/terms" false))
            _prev (if-let [p (and bindings (-> bindings (.getBinding "prev")))]
                    (.getValue p)
                    doc)
            _next (if-let [n (and bindings (-> bindings (.getBinding "next")))]
                    (.getValue n)
                    RDF/NIL)
            _list (node)]
        (-> conn
          (remove-statements _prev RDF/REST nil (uri resources))
          (add-statement _prev RDF/REST _list (uri resources)))
        (let [_end (-> conn (append-items _list cards (uri resources)))]
          (-> conn
            (add-statement _end RDF/REST _next (uri resources)))
          (-> conn .commit))
        doc
        ))))


(defn document!
  [title & cards]
  (with-sc conn
    (let [doc (indv resources "documents")]
      (-> conn
        (add-statement doc RDF/TYPE (lt :Document) (uri resources))
        (add-statement doc RDFS/LABEL (literal title) (uri resources))
        (append-items doc cards (uri resources)))
      (-> conn .commit)
      doc)))


(defn documents
  [& [inf]]
  (with-sc conn
    (doall
      (map
        (fn [x]
          (let [doc (-> x (.getBinding "doc") .getValue)]
            {:uri doc
             :title (-> x (.getBinding "title") .getValue)}))
        (-> conn
          (query
             (str "select ?doc ?title "
                  "from <http://linnea.nclk.co/resources> "
                  "where { ?doc a <#Document> . ?doc rdfs:label ?title . }")
             "http://linnea.nclk.co/terms" inf))))))



;;(defn query
;;  [file]
;;  (let [query (slurp file)
;;        ;resp (client/get (str "http://wiktionary.dbpedia.org/sparql?format=json&default-graph-uri=&query=" (java.net.URLEncoder/encode query)))]
;;        resp (client/get (str "http://kaiko.getalp.org/sparql?format=json&default-graph-uri=&query=" (java.net.URLEncoder/encode query)))]
;;    (clojure.pprint/pprint (-> resp :body str (cheshire.core/parse-string true)))
;;    ))



