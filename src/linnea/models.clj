(ns linnea.models
  (:require [clojure.java.jdbc :as j]
            [cheshire.core :as json]
            [me.raynes.conch :as sh]
            [clojure.core.async :refer [go >! <!! chan]]
            [clj-http.client :as client]))


(def ^:dynamic *db* nil)
(def google-api-key "AIzaSyA6Dqpn-wlNTJbjIQ_u4DmrceyMoLT5bUc")
(def yandex-api-key "trnsl.1.1.20150913T155020Z.f35e0e0e6d64a47d.4e708c9d0745a038e5beae2bf6fdad1356d99cc3")
(def yandex_translate-base (str "https://translate.yandex.net/api/v1.5/tr.json/translate?key=" yandex-api-key "&"))


(defn audio-bytes-from-source
  [source]
  (do
    (sh/let-programs [speak (-> "speak" clojure.java.io/resource .toURI java.io.File. str)]
      (speak "-v" "sv" "-w" ".tmp.wav" "-s" "100" source))
    (let [tmp (java.io.File. ".tmp.wav")
          b (-> tmp .toPath java.nio.file.Files/readAllBytes)]
      (.delete tmp)
      b)))


(defn add-card
  [source & [audio translation]]
  (j/with-db-transaction [tr *db*]
    (let [rows (j/insert! tr :card
                 {:source source
                  :audio (if audio
                           (-> (java.io.File. audio) .toPath java.nio.file.Files/readAllBytes)
                           (audio-bytes-from-source source))})]
      (j/insert! tr :translation
        {:card_id (-> rows first :id)
         :translation (if translation
                        translation
                        (let [resp (client/get (str yandex_translate-base "lang=sv-en&text=" source) {:accept :json})]
                          (println (-> resp :body))
                          (-> resp :body (json/parse-string true) :text first)
                          )
                        )}))))


(defn add-card-from-target
  [target]
  (j/with-db-transaction [tr *db*]
    (let [rows (j/insert! tr :card
                 (let [source (-> (client/get (str yandex_translate-base "lang=en-sv&text=" target) {:accept :json})
                                  :body
                                  (json/parse-string true)
                                  :text
                                  first)
                       audio (audio-bytes-from-source source)]
                   {:source source :audio audio}))]
      (j/insert! tr :translation
        {:card_id (-> rows first :id)
         :translation target}))))


(defn assoc-audio
  [card]
  (assoc card :audio
    (-> (j/query *db* ["select audio from card where id = ?" (-> card :id)])
      first :audio)))


(defn random-cards
  [& {:keys [exclude limit audio]}]
  (let [exclude (or (and (not (empty? exclude))
                         exclude)
                    [])
        limit (and (number? limit) limit)
        query (concat [(str "select id, source "
                            (when audio ", audio ")
                            "from card "
                            (when-not (empty? exclude)
                              (str "where id not in ("
                                   (clojure.string/join "," (take (count exclude) (repeat "?")))
                                   ") "))
                            "order by random() "
                            (when limit "limit ? "))]
                      (or exclude [])
                      (when limit [limit]))
        rows (j/query *db* query)]
     (when-not (empty? rows)
       (for [row rows]
         (assoc row :translations
           (j/query *db* ["select * from translation where card_id = ?" (-> row :id)]))))))

