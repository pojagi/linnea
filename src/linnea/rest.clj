(ns linnea.rest
  (:require [compojure.core :refer :all]
            [compojure.route :refer [not-found resources]]
            [linnea.models :as models]
            [cheshire.core :as json]
            [ring.middleware.defaults :refer [wrap-defaults api-defaults site-defaults]]
            ))


;; TODO: 
;; create a client

(defroutes db-routes
  (context "/cards" request
    (GET "/random" request
      (let [body (-> request :body clojure.java.io/reader (json/parse-stream true))
            exclude (-> body :exclude)
            limit (-> body :limit)]
        (json/generate-string
          (if-let [card (models/random-cards :exclude exclude :limit limit)]
            card
            {}))))))


(defn wrap-db
  [handler]
  (fn [request]
    (binding [models/*db* "postgresql://localhost:5432/linnea"]
      (handler request))))


(defroutes main
  (context "/api" request
    (context "/v1" request
      (wrap-db db-routes)
      )))


(def app
  (-> main
    (wrap-defaults api-defaults)
    (wrap-defaults site-defaults)
    ))
